# Mysterious Incidents

This is a small team project in which we developed a game where you are playing as a detective and have to solve a case.
The level we developed guides you over an island where a person went missing.
You have to interrogate other individuals and find hints/clues to complete the main goal.

### Video Showcase
[![Showcase](https://www.youtube.com/watch?v=0g0uupm0uKg/0.jpg)](https://www.youtube.com/watch?v=0g0uupm0uKg.mp4)