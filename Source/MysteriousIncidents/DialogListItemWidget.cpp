// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogListItemWidget.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"

#include "DialogMenuWidget.h"

void UDialogListItemWidget::Setup(UDialogMenuWidget* InParent, uint32 Idx, FString DialogName)
{
	Parent = InParent;

	Index = Idx;

	if (!ensure(TextBlock_DialogName != nullptr)) return;
	TextBlock_DialogName->SetText(FText::FromString(DialogName));
}

bool UDialogListItemWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(Button_SelectItem != nullptr)) { return false; }
	Button_SelectItem->OnClicked.AddDynamic(this, &UDialogListItemWidget::SelectItem);

	return true;
}

void UDialogListItemWidget::SelectItem()
{
	if (!ensure(Parent != nullptr)) return;

	Parent->DisplayDialog(Index);
}