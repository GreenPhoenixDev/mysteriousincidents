// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "DialogListItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UDialogListItemWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void Setup(class UDialogMenuWidget* InParent, uint32 Idx, FString DialogName);
	
protected:
	virtual bool Initialize() override;

private:
	UFUNCTION()
		void SelectItem();

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* TextBlock_DialogName;
	UPROPERTY(meta = (BindWidget))
		class UButton* Button_SelectItem;

	class UDialogMenuWidget* Parent{ nullptr };
	uint32 Index;
};
