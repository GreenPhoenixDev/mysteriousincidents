// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogMenuWidget.h"

#include "DialogListItemWidget.h"
#include "NPCDialogTableStructure.h"
#include "NPC.h"
#include "NPCDialogBT.h"

#include "Components/TextBlock.h"
#include "Components/ScrollBox.h"

void UDialogMenuWidget::SetNPC(ANPC* IC)
{
	TalkingNPC = IC;

	if (!ensure(TextBlock_CharacterName != nullptr)) return;

	if (TalkingNPC != nullptr)
		TextBlock_CharacterName->SetText(FText::FromString(TalkingNPC->GetName()));
	else
		TextBlock_CharacterName->SetText(FText::FromString(TEXT("Talking Character is NULL!")));

	FillDialogList();
}

void UDialogMenuWidget::FillDialogList()
{
	if (!ensure(TalkingNPC != nullptr)) return;

	Dialogs = TalkingNPC->GetUnlockedDialogs();

	for (size_t i = 0; i < Dialogs.Num(); i++)
	{
		if (!ensure(ScrollBox_DialogList != nullptr)) return;

		UDialogListItemWidget* DialogItem = CreateWidget<UDialogListItemWidget>(GetWorld(), DialogListItemClass);
		if (!ensure(DialogItem != nullptr)) continue;

		if (!ensure(Dialogs[i].DialogTree != nullptr)) return;
		DialogItem->Setup(this, i, Dialogs[i].DialogName);

		ScrollBox_DialogList->AddChild(DialogItem);
	}

	if (Dialogs.Num() > 0)
		DisplayDialog(0);
}

void UDialogMenuWidget::DisplayDialog(uint8 Idx)
{
	TalkingNPC->RunCurrentBehaviorTree(Idx);
}

void UDialogMenuWidget::SetText(FString& NewText)
{
	if (!ensure(TextBlock_DialogText != nullptr)) return;
	TextBlock_DialogText->SetText(FText::FromString(NewText));
}