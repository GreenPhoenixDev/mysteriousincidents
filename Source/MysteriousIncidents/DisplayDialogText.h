// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "DisplayDialogText.generated.h"

/**
 *
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UDisplayDialogText : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
	UPROPERTY(EditAnywhere)
		TArray<FString> DialogTextParts {"Missing Text"};
};
