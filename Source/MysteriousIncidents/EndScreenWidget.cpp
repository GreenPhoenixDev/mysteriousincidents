// Fill out your copyright notice in the Description page of Project Settings.


#include "EndScreenWidget.h"

#include "Components/Button.h"

bool UEndScreenWidget::Initialize()
{
	if(!Super::Initialize()) return false;

	if (!ensure(Button_LoadMainMenu != nullptr)) return false;
	Button_LoadMainMenu->OnClicked.AddDynamic(this, &UEndScreenWidget::LoadMainMenu);

	return true;
}

void UEndScreenWidget::LoadMainMenu()
{
	if (!ensure(MenuInterface != nullptr)) return;

	MenuInterface->LoadLevelAsync("MainMenu");
}