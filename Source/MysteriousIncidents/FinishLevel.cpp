// Fill out your copyright notice in the Description page of Project Settings.


#include "FinishLevel.h"

#include "Kismet/GameplayStatics.h"
#include "MysteriousIncidentsGameInstance.h"

EBTNodeResult::Type UFinishLevel::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UGameInstance* GameInst = UGameplayStatics::GetGameInstance(GetWorld());
	if (!ensure(GameInst != nullptr)) return EBTNodeResult::Failed;

	UMysteriousIncidentsGameInstance* Inst = Cast<UMysteriousIncidentsGameInstance>(GameInst);
	if (!ensure(Inst != nullptr)) return EBTNodeResult::Failed;

	Inst->LoadLevelAsync("EndScreen");

	return EBTNodeResult::Succeeded;
}