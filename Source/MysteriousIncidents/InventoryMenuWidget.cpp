// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryMenuWidget.h"

#include "Components/ScrollBox.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"
#include "MysteriousIncidentsGameInstance.h"

#include "MainCharacter.h"
#include "PickUpItem.h"
#include "MCInventoryComponent.h"
#include "InventoryItemWidget.h"
#include "QuestManager.h"
#include "Quest.h"

void UInventoryMenuWidget::Setup()
{
	UToggleMenuWidget::Setup();

	SetItems();
	SetObjectiveText();
	FillScrollBox();
}

void UInventoryMenuWidget::OnLevelRemovedFromWorld(ULevel* Level, UWorld* World)
{
	Super::OnLevelRemovedFromWorld(Level, World);

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController == nullptr) return;

	FInputModeUIOnly InputModeData;

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = true;
}

void UInventoryMenuWidget::SetItems()
{
	APawn* Pawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (!ensure(Pawn != nullptr)) return;

	AMainCharacter* Player = Cast<AMainCharacter>(Pawn);
	if (!ensure(Player != nullptr)) return;

	UMCInventoryComponent* Inventory = Player->GetInventoryComponent();
	if (!ensure(Inventory != nullptr)) return;

	Items = Inventory->GetItems();
}

void UInventoryMenuWidget::SetObjectiveText()
{
	UGameInstance* GameInst = UGameplayStatics::GetGameInstance(GetWorld());
	if (!ensure(GameInst != nullptr)) return;

	UMysteriousIncidentsGameInstance* Inst = Cast<UMysteriousIncidentsGameInstance>(GameInst);
	if (!ensure(Inst != nullptr)) return;

	AQuestManager* QuestManager = Inst->GetCurrentQuestManager();
	if (!ensure(QuestManager != nullptr)) return;

	UQuest* Quest = QuestManager->GetCurrentQuest();
	if (Quest == nullptr) return;						// dont do an advanced check here because there could be no active quest at some time

	if (!ensure(Text_ObjectiveDescription != nullptr)) return;
	TArray<FString> ObjectiveDescription = Quest->GetCurrentObjectiveDescription();
	FString AppendedString{ "" };
	for (auto Text : ObjectiveDescription)
	{
		AppendedString += Text + "\n";
	}

	Text_ObjectiveDescription->SetText(FText::FromString(AppendedString));
}

void UInventoryMenuWidget::FillScrollBox()
{
	if (!ensure(ScrollBox_InventorySlots != nullptr)) return;

	for (size_t i = 0; i < Items.Num(); i++)
	{
		UInventoryItemWidget* NewItem = CreateWidget<UInventoryItemWidget>(GetWorld(), InventoryItemClass);
		if (!ensure(NewItem != nullptr)) return;

		NewItem->Setup(this, i, Items[i]);

		ScrollBox_InventorySlots->AddChild(NewItem);
	}
}

void UInventoryMenuWidget::DisplaySelectedItemInfo(int Idx)
{
	if (!ensure(Items[Idx] != nullptr)) return;

	TArray<FString> ItemDescription = Items[Idx]->GetItemDescription();
	FString AppendedString{ "" };
	for (auto Text : ItemDescription)
	{
		AppendedString += Text + "\n";
	}

	Text_ItemDescription->SetText(FText::FromString(AppendedString));
}