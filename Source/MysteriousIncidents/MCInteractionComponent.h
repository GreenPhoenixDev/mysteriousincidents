// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MCMasterActorComponent.h"
#include "MCInteractionComponent.generated.h"

/**
 *
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API UMCInteractionComponent : public UMCMasterActorComponent
{
	GENERATED_BODY()

public:
	UMCInteractionComponent() {}

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION()
		void OnBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
		void TryInteract();

	void Grab(class APickUpItem* Item);
	void Talk(class ANPC* NPC);

private:
	void AddNewInteractableInRange(class IInteractable* Interactable);
	void RemoveInteractableInRange(class IInteractable* Interactable);
	class IInteractable* GetNearestInteractable();

	TArray<class IInteractable*> InteractablesInRange;
};
