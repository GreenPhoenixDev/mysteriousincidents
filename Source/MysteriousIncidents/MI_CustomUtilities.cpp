// Fill out your copyright notice in the Description page of Project Settings.


#include "MI_CustomUtilities.h"

MI_CustomUtilities::MI_CustomUtilities(){}
MI_CustomUtilities::~MI_CustomUtilities(){}

FString MI_CustomUtilities::FormatFloatString(float Value, int32 Precision)
{
	FNumberFormattingOptions NF;
	NF.MinimumIntegralDigits = 1;
	NF.MinimumFractionalDigits = Precision;
	NF.MaximumFractionalDigits = Precision;

	return FText::AsNumber(Value, &NF).ToString();
}

FText MI_CustomUtilities::FormatFloatText(float Value, int32 Precision)
{
	FNumberFormattingOptions NF;
	NF.MinimumIntegralDigits = 1;
	NF.MinimumFractionalDigits = Precision;
	NF.MaximumFractionalDigits = Precision;

	return FText::AsNumber(Value, &NF);
}