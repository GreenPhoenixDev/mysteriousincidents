// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class MYSTERIOUSINCIDENTS_API MI_CustomUtilities
{
public:
	MI_CustomUtilities();
	~MI_CustomUtilities();

	static FString FormatFloatString(float Value, int32 Precision);
	static FText FormatFloatText(float Value, int32 Precision);
};
