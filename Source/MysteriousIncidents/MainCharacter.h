// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MysteriousIncidentsCharacter.h"
#include "MainCharacter.generated.h"

/**
 *
 */
UCLASS()
class MYSTERIOUSINCIDENTS_API AMainCharacter : public AMysteriousIncidentsCharacter
{
	GENERATED_BODY()

public:
	AMainCharacter();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	void SetLookUpSensitivity(float Value) {};
	void SetTurnSensitivity(float Value) {};

protected:
	void MoveForward(float Value);
	void MoveRight(float Value);
	void Sprint();
	void StopSprint();

public:
	// Component Getters
	class UMCInteractionComponent* GetInteractionComponent() { return InteractionComponent; }
	class UMCInventoryComponent* GetInventoryComponent() { return InventoryComponent; }
	class UMCMovementComponent* GetAdvancedMovementComponent() { return AdvancedMovementComponent; }
	class UCapsuleComponent* GetItemCollisionCapsule() { return ItemCollisionCapsule; }
	class UCameraComponent* GetCamera() { return GetFollowCamera(); }

private:
	 //Components
	UPROPERTY(VisibleAnywhere)
		class UMCInteractionComponent* InteractionComponent{ nullptr };
	UPROPERTY(VisibleAnywhere)
		class UMCInventoryComponent* InventoryComponent{ nullptr };
	UPROPERTY(EditAnywhere)
		class UMCMovementComponent* AdvancedMovementComponent{ nullptr };
	UPROPERTY(EditAnywhere)
		class UCapsuleComponent* ItemCollisionCapsule{ nullptr };
};
