// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"

#include "MainMenuWidget.generated.h"

UCLASS()
class MYSTERIOUSINCIDENTS_API UMainMenuWidget : public UMenuWidget
{
	GENERATED_BODY()

protected:
	virtual bool Initialize() override;
	virtual void OnLevelRemovedFromWorld(ULevel* Level, UWorld* W);

private:
	UFUNCTION()
		void SwitchToLoadMenu();
	UFUNCTION()
		void SwitchToStartingMenu();
	UFUNCTION()
		void QuitApplication();
	UFUNCTION()
		void StartNewGame();
	UFUNCTION()
		void LoadSavedGame();

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_SwitchToLoadMenu;

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_BackFromLoad;

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_QuitApplication;

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_StartNewGame;

	UPROPERTY(meta = (BindWidget))
		class UWidgetSwitcher* MenuSwitcher;

	UPROPERTY(meta = (BindWidget))
		class UWidget* LoadMenu;

	UPROPERTY(meta = (BindWidget))
		class UWidget* StartingMenu;

	UPROPERTY(meta = (BindWidget))
		class UPanelWidget* ScrollBox_SavedGamesList;

	//UPROPERTY(EditAnywhere, Category = DefaultValues)
	//	TSubclassOf<class UUserWidget> SaveGameClass;

	TOptional<uint32> SelectedIndex;
};
