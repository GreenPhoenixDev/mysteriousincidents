// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MysteriousIncidents : ModuleRules
{
	public MysteriousIncidents(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "AIModule", "GameplayTasks" });
	}
}
