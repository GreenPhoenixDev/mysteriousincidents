// Copyright Epic Games, Inc. All Rights Reserved.

#include "MysteriousIncidents.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MysteriousIncidents, "MysteriousIncidents" );
 