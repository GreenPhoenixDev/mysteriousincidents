// Copyright Epic Games, Inc. All Rights Reserved.

#include "MysteriousIncidentsGameMode.h"

#include "MysteriousIncidentsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMysteriousIncidentsGameMode::AMysteriousIncidentsGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
