// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MysteriousIncidentsGameMode.generated.h"

UCLASS(minimalapi)
class AMysteriousIncidentsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMysteriousIncidentsGameMode();
};



