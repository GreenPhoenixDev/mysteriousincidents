// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BehaviorTree.h"
#include "NPCDialogBT.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class MYSTERIOUSINCIDENTS_API UNPCDialogBT : public UBehaviorTree
{
	GENERATED_BODY()
	
public:
	bool GetIsUnlocked() { return IsUnlocked; }
	FString& GetName() { return DialogName; }

private:
	UPROPERTY(EditAnywhere)
		bool IsUnlocked{ false };
	UPROPERTY(EditAnywhere)
		FString DialogName {"Unnanmed"};
};
