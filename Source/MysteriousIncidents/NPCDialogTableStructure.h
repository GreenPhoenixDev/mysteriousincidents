// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"

#include "NPCDialogTableStructure.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct MYSTERIOUSINCIDENTS_API FNPCDialogTableStructure : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
	//	int RowNumber;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		int NPC_ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		int Dialog_ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		int Page_ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FString Dialog_Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Struct")
		FString Text;
};
