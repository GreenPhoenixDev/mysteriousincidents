// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "NPCManager.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYSTERIOUSINCIDENTS_API ANPCManager : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	ANPCManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	TArray<class ANPC*>& GetNPCs() { return NPCs; }

private:
	UPROPERTY(EditAnywhere)
		TArray<class ANPC*> NPCs;
};
