// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "StructCollection.h"

#include "Quest.generated.h"

UCLASS(BlueprintType, Blueprintable)
class MYSTERIOUSINCIDENTS_API UQuest : public UObject
{
	GENERATED_BODY()

public:
	bool CountDownItems(class APickUpItem* InItem);
	bool CountDownObjectiveCharacters(class ANPC* InNPC, uint8 InDialogID);
	void Setup(const FQuestBase& InQuestBase, class UMysteriousIncidentsGameInstance* InstRef);

	const FString& GetQuestName() { return QuestBase.QuestName; }
	const TArray<FString>& GetQuestDescription() { return QuestBase.QuestDescription; }
	const TArray<FString>& GetCurrentObjectiveDescription() { return QuestBase.ObjectivesInOrder[CurrentObjectiveIndex].ObjectiveDescription; }

private:
	void UnlockObjectiveItems(const TArray<uint8>& ItemsToUnlock);
	void UnlockItems(const TArray<uint8>& ItemsToUnlock);
	void UnlockDialogs(const TArray<FCharacterDialog>& DialogsToUnlock);
	void UnlockObjectiveDialogs(const TArray<FCharacterDialog>& DialogsToUnlock);
	void StartObjective();
	void CheckObjectiveFinished();
	void OnObjectiveFinished();
	void OnQuestFinished();

	UPROPERTY(VisibleAnywhere)
		FQuestBase QuestBase;

	UPROPERTY(VisibleAnywhere)
		TArray<class APickUpItem*> ObjectiveItems;
	UPROPERTY(VisibleAnywhere)
		TArray<FObjectiveCharacter> ObjectiveCharacterDialogs;

	uint8 CurrentObjectiveIndex{ 0 };
	class UMysteriousIncidentsGameInstance* Inst{ nullptr };
};
