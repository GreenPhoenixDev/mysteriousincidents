// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "QuestManager.generated.h"

UCLASS()
class MYSTERIOUSINCIDENTS_API AQuestManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AQuestManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	void CreateQuest(struct FQuestBase& RawQuest);
	void FinishedQuest(class UQuest* InFinishedQuest);
	UQuest* GetCurrentQuest() { return ActiveQuests.IsValidIndex(0) ? ActiveQuests[0] : nullptr; }

private:
	UPROPERTY(VisibleAnywhere)
		TArray<class UQuest*> ActiveQuests;
	UPROPERTY(VisibleAnywhere)
		TArray<class UQuest*> FinishedQuests;
};
