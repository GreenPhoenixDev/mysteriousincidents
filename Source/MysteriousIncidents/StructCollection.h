// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "StructCollection.generated.h"

class MYSTERIOUSINCIDENTS_API StructCollection
{
	//GENERATED_BODY()

public:
	StructCollection(){}
	~StructCollection(){}
};

USTRUCT()
struct MYSTERIOUSINCIDENTS_API FCharacterDialog
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere)
		uint8 Character_ID {0};
	UPROPERTY(EditAnywhere)
		uint8 Dialog_ID {0};
};

USTRUCT()
struct MYSTERIOUSINCIDENTS_API FMiniObjective
{
	GENERATED_USTRUCT_BODY()

public:
	// This is the DESCRIPTION of this objective which will be displayed in the UI.
	UPROPERTY(EditAnywhere)
		TArray<FString> ObjectiveDescription {"Missing Objective Description"};

	// These are the ITEMS which will be unlocked immediately and are a part of this objective.
	UPROPERTY(EditAnywhere, DisplayName = "Items to look for")
		TArray<uint8> ObjectiveItems;

	// These are the DIALOGS which will be unlocked immediately and are a part of this objective.
	UPROPERTY(EditAnywhere, DisplayName = "Dialogs that need to be conducted")
		TArray<FCharacterDialog> ObjectiveDialogs;

	//UPROPERTY(EditAnywhere)
	//	TArray<FCharacterDialog> DialogsToUnlockOnObjectiveFinished;

	//// Type the ID of the item you want to unlock in here.
	//UPROPERTY(EditAnywhere)
	//	TArray<uint8> ItemsToUnlockOnObjectiveFinished;
};

USTRUCT()
struct MYSTERIOUSINCIDENTS_API FQuestBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere)
		uint8 QuestID {0};
	UPROPERTY(EditAnywhere)
		FString QuestName {"Unnamed Quest"};
	UPROPERTY(EditAnywhere)
		TArray<FString> QuestDescription {"Blank Quest Description"};
	UPROPERTY(EditAnywhere, DisplayName = "Quest parts that are followed in order")
		TArray<FMiniObjective> ObjectivesInOrder;
};

USTRUCT()
struct MYSTERIOUSINCIDENTS_API FObjectiveCharacter
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(VisibleAnywhere)
		class ANPC* NPC{ nullptr };
	UPROPERTY(VisibleAnywhere)
		uint8 DialogID {0};
};

USTRUCT()
struct MYSTERIOUSINCIDENTS_API FDialogStruct
{
	GENERATED_USTRUCT_BODY()

public:
	// This is the DIALOGNAME which will be displayed in the UI when talking to a NPC;
	UPROPERTY(EditAnywhere)
		FString DialogName {"Unnamed Dialog"};

	// This is the DIALOGTREE which will be used when it is selected.
	UPROPERTY(EditAnywhere)
		class UBehaviorTree* DialogTree;
};

FORCEINLINE bool operator==(const FDialogStruct& Dialog1, const FDialogStruct& Dialog2)
{
	return (Dialog1.DialogName == Dialog2.DialogName && Dialog1.DialogTree == Dialog2.DialogTree);
}