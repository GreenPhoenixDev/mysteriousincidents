// Fill out your copyright notice in the Description page of Project Settings.

#include "ToggleMenuWidget.h"

#include "Components/Button.h"

void UToggleMenuWidget::CloseMenu()
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	FInputModeGameOnly InputModeData;

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = false;

	this->bIsFocusable = false;
	this->RemoveFromViewport();
}

bool UToggleMenuWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(Button_CloseMenu != nullptr)) return false;
	Button_CloseMenu->OnClicked.AddDynamic(this, &UToggleMenuWidget::CloseMenu);

	return true;
}

void UToggleMenuWidget::Setup()
{
	UMenuWidget::Setup();
}